.
# Hinweis for die Darstellung in gitlab
Diese Page ist für Doxygen geschrieben. Den vollständigen Output ist in den Artefacts zu finden.

# 1. Systemübersicht #
Die konkreten funktionalen Anforderungen an die einzelnen Use Cases sind im Abschnitt 4 auf Seite 10 aufgeführt.
Das folgende Diagramm zeigt die Funktionen des Systems:
@image html Kontextdiagramm.gif "Kontext-Diagramm"
Im vorliegenden Projekt können alle Grafikelemente vom Qt-Framework verwendet werden. Eine Darstellung mit typischen Elementen ist ausreichend. Da das Programm mit Qt implementiert werden muss ist das Software Interface durch die Qt API für den Entwickler gegeben. Das Programm selbst ist in sich geschlossen und es wird keine Software API angeboten. Das Graphical User Interface (GUI) ist die einzige zugängliche Schnittstelle zum Qt Spannungsteiler Programm. Der User kann über das GUI die unter Abschnitt 2 @ref product_function aufgelisteten Parameter konfigurieren und die Widerstände berechnen lassen. Das Resultat der Widerstandsberechnung wird ebenfalls auf dem GUI angezeigt.

# 2. Produktfunktionen #
\anchor product_function
Die folgenden Parameter können über das User-Interface konfiguriert werden:
-	Eingangsspanung \f$U1\f$,
-	Ausgangsspannung \f$U2\f$,
-	Minimaler Widerstandswert \f$Rmin\f$
-	Maximaler Widerstandwert \f$Rmax\f$
-	Widerstands-E-Reihe

Sofern ein Resultat gefunden werden konnte werden folgende Angaben auf dem GUI dargestellt:
-	\f$R1\f$
-	\f$R2\f$
-	Resultierende Ausgangsspannung
-	Abweichung zur erwarteten Ausgangsspannung

@image html vDivider.gif "Unloaded Voltage Divider"
