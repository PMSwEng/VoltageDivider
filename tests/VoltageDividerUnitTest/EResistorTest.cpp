

#include <gtest/gtest.h>
#include "../../src/EResistor.h"
#include "ESeriesMock.h"

TEST(EResistorTest, assignUpperVTest_1)
{
  double values[] = {1.00, 1.20};
  ESeriesMock eSerie;
  eSerie.setElements(values, sizeof(values) / sizeof(values[0]));
  EResistor eRes(1, EResistor::assignUpperEValue, eSerie);
  EXPECT_EQ(eRes.getValue(), 1);
}

TEST(EResistorTest, assignUpperVTest_2)
{
  double values[] = {1.00, 1.20};
  ESeriesMock eSerie;
  eSerie.setElements(values, sizeof(values) / sizeof(values[0]));
  EResistor eRes(nextafter(1, 2), EResistor::assignUpperEValue, eSerie);
  EXPECT_EQ(eRes.getValue(), 1.2);
}

TEST(EResistorTest, assignUpperITest)
{
  double values[] = {1.00, 1.20};
  ESeriesMock eSerie;
  eSerie.setElements(values, sizeof(values) / sizeof(values[0]));
  EXPECT_DEATH(EResistor eRes(0, EResistor::assignUpperEValue, eSerie), "");
}

TEST(EResistorTest, assignLowerTest_1)
{
  double values[] = {1.00, 1.20};
  ESeriesMock eSerie;
  eSerie.setElements(values, sizeof(values) / sizeof(values[0]));
  EResistor eRes(1.2, EResistor::assignLowerEValue, eSerie);
  EXPECT_EQ(eRes.getValue(), 1.2);
}

TEST(EResistorTest, assignLowerTest_2)
{
  double values[] = {1.00, 1.20};
  ESeriesMock eSerie;
  eSerie.setElements(values, sizeof(values) / sizeof(values[0]));
  EResistor eRes(nextafter(1.2, 1), EResistor::assignLowerEValue, eSerie);
  EXPECT_EQ(eRes.getValue(), 1.0);
}

TEST(EResistorTest, assignLowerITest)
{
  double values[] = {1.00, 1.20};
  ESeriesMock eSerie;
  eSerie.setElements(values, sizeof(values) / sizeof(values[0]));
  EXPECT_DEATH(EResistor eRes(0, EResistor::assignLowerEValue, eSerie), "");
}

TEST(EResistorTest, greaterOperatorVTest)
{
  double values[] = {1.00, 1.20};
  ESeriesMock eSerie;
  eSerie.setElements(values, sizeof(values) / sizeof(values[0]));
  EResistor eRes1(1, EResistor::assignLowerEValue, eSerie);
  EResistor eRes2(1.2, EResistor::assignLowerEValue, eSerie);
  EXPECT_TRUE(eRes2 > eRes1);
}

TEST(EResistorTest, greaterOperatorITest)
{
  double values[] = {1.00, 1.20};
  ESeriesMock eSerie;
  eSerie.setElements(values, sizeof(values) / sizeof(values[0]));
  EResistor eRes1(1, EResistor::assignLowerEValue, eSerie);
  EResistor eRes2(1.2, EResistor::assignLowerEValue, eSerie);
  EXPECT_FALSE(eRes1 > eRes2);
}

TEST(EResistorTest, greaterEqOperatorVTest_1)
{
  double values[] = {1.00, 1.20};
  ESeriesMock eSerie;
  eSerie.setElements(values, sizeof(values) / sizeof(values[0]));
  EResistor eRes1(1, EResistor::assignLowerEValue, eSerie);
  EResistor eRes2(1, EResistor::assignLowerEValue, eSerie);
  EXPECT_TRUE(eRes2 >= eRes1);
}

TEST(EResistorTest, greaterEqOperatorVTest_2)
{
  double values[] = {1.00, 1.20};
  ESeriesMock eSerie;
  eSerie.setElements(values, sizeof(values) / sizeof(values[0]));
  EResistor eRes1(1, EResistor::assignLowerEValue, eSerie);
  EResistor eRes2(1.2, EResistor::assignLowerEValue, eSerie);
  EXPECT_TRUE(eRes2 >= eRes1);
}

TEST(EResistorTest, greaterEqOperatorITest)
{
  double values[] = {1.00, 1.20};
  ESeriesMock eSerie;
  eSerie.setElements(values, sizeof(values) / sizeof(values[0]));
  EResistor eRes1(1, EResistor::assignLowerEValue, eSerie);
  EResistor eRes2(1.2, EResistor::assignLowerEValue, eSerie);
  EXPECT_FALSE(eRes1 >= eRes2);
}

TEST(EResistorTest, smallerOperatorVTest)
{
  double values[] = {1.00, 1.20};
  ESeriesMock eSerie;
  eSerie.setElements(values, sizeof(values) / sizeof(values[0]));
  EResistor eRes1(1, EResistor::assignLowerEValue, eSerie);
  EResistor eRes2(1.2, EResistor::assignLowerEValue, eSerie);
  EXPECT_TRUE(eRes1 < eRes2);
}

TEST(EResistorTest, smallerOperatorITest)
{
  double values[] = {1.00, 1.20};
  ESeriesMock eSerie;
  eSerie.setElements(values, sizeof(values) / sizeof(values[0]));
  EResistor eRes1(1, EResistor::assignLowerEValue, eSerie);
  EResistor eRes2(1.2, EResistor::assignLowerEValue, eSerie);
  EXPECT_FALSE(eRes2 < eRes1);
}

TEST(EResistorTest, smallerEqOperatorVTest_1)
{
  double values[] = {1.00, 1.20};
  ESeriesMock eSerie;
  eSerie.setElements(values, sizeof(values) / sizeof(values[0]));
  EResistor eRes1(1, EResistor::assignLowerEValue, eSerie);
  EResistor eRes2(1, EResistor::assignLowerEValue, eSerie);
  EXPECT_TRUE(eRes1 <= eRes2);
}

TEST(EResistorTest, smallerEqOperatorVTest_2)
{
  double values[] = {1.00, 1.20};
  ESeriesMock eSerie;
  eSerie.setElements(values, sizeof(values) / sizeof(values[0]));
  EResistor eRes1(1, EResistor::assignLowerEValue, eSerie);
  EResistor eRes2(1.2, EResistor::assignLowerEValue, eSerie);
  EXPECT_TRUE(eRes1 <= eRes2);
}

TEST(EResistorTest, smallerEqOperatorITest)
{
  double values[] = {1.00, 1.20};
  ESeriesMock eSerie;
  eSerie.setElements(values, sizeof(values) / sizeof(values[0]));
  EResistor eRes1(1, EResistor::assignLowerEValue, eSerie);
  EResistor eRes2(1.2, EResistor::assignLowerEValue, eSerie);
  EXPECT_FALSE(eRes2 <= eRes1);
}

TEST(EResistorTest, preIncOperator)
{
  double values[] = {1.00, 1.20};
  ESeriesMock eSerie;
  eSerie.setElements(values, sizeof(values) / sizeof(values[0]));
  EResistor eRes(1, EResistor::assignLowerEValue, eSerie);
  EXPECT_EQ(++eRes, 1.2);
}

TEST(EResistorTest, multOperator)
{
  double values[] = {1.00, 1.20};
  ESeriesMock eSerie;
  eSerie.setElements(values, sizeof(values) / sizeof(values[0]));
  EResistor eRes1(1, EResistor::assignLowerEValue, eSerie);
  EResistor eRes2(1.2, EResistor::assignLowerEValue, eSerie);
  EXPECT_EQ(eRes2 * eRes1, 1.2);
}

TEST(EResistorTest, addOperator)
{
  double values[] = {1.00, 1.20};
  ESeriesMock eSerie;
  eSerie.setElements(values, sizeof(values) / sizeof(values[0]));
  EResistor eRes1(1, EResistor::assignLowerEValue, eSerie);
  EResistor eRes2(1.2, EResistor::assignLowerEValue, eSerie);
  EXPECT_EQ(eRes2 + eRes1, 2.2);
}
