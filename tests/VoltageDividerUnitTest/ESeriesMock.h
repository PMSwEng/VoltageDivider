#ifndef ESERIESMOCK_H
#define ESERIESMOCK_H

#include <cassert>
#include "../../src/IESeries.h"

///
/// \brief The ESeriesMock class is used to mock the real ESeries class for test
/// reasons.
///
class ESeriesMock : public IESeries {
 public:
  ///
  /// \brief ESeriesMock Ctor. For the mock no parameter required
  ///
  ESeriesMock() : values(0), len(0)
  {
  }

  ///
  /// \brief ESeriesMock Ctor. This Ctor is used for compatibility to the real
  ///  ESeries Ctor
  ///
  ESeriesMock(IESeries::SeriesType serie) : values(0), len(0)
  {
    (void) serie;
  }

  ///
  /// \brief getElemCnt function returns the count of desired E series. E.g. an
  /// object generated as E12 object returns 12.
  /// \return count of elements in represented E series
  ///
  virtual unsigned int getElemCnt() const
  {
    return len;
  }

  ///
  /// \brief getName function returns the string of the desired E series. E.g.
  /// "E12" for an E12 serie.
  /// \return
  ///
  virtual std::string getName() const
  {
    return "dummy";
  }

  ///
  /// \brief operator [] to get an element of the serie with an index.
  /// \pre index < size of serie (element count)
  /// \pre index >= 0
  /// \param index of the element to be returned
  /// \return the element that at the index position in the serie represented
  ///         by the object
  ///
  virtual double operator[](int index) const
  {
    assert(index >= 0);
    assert(index < static_cast<int>(len));
    return values[index];
  }

  ///
  /// \brief getType returns used E series type
  /// \return type of SeriesType
  ///
  virtual SeriesType getType() const
  {
    return IESeries::e12;
  }

  ///
  /// \brief setElements is a specific mock function to insert E serie values
  /// \param values vector to be set
  /// \param len of the values vector
  ///
  void setElements(double values[], size_t len)
  {
    this->values = values;
    this->len = len;
  }

 private:
  double* values;
  size_t len;
};

#endif  // ESERIESMOCK_H
