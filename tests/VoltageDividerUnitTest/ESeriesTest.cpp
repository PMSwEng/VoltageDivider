///
/// \file ESeriesTest.cpp
/// \date 30.11.2016
/// \author Michael Trummer
///

#include <gtest/gtest.h>
#include "../../src/ESeries.h"

///
/// \brief E12LengthAndValues
/// \test  check if E12 resistor decade values and length are correct
///
TEST(ESeriesTest, E12LengthAndValues)
{
  ESeries serie(ESeries::e12);

  EXPECT_EQ(serie.getElemCnt(), 12);

  EXPECT_EQ(serie.getName(), "E12");

  EXPECT_TRUE(1.00 == serie[0]);
  EXPECT_EQ(serie[1], 1.20);
  EXPECT_EQ(serie[2], 1.50);
  EXPECT_EQ(serie[3], 1.80);
  EXPECT_EQ(serie[4], 2.20);
  EXPECT_EQ(serie[5], 2.70);
  EXPECT_EQ(serie[6], 3.30);
  EXPECT_EQ(serie[7], 3.90);
  EXPECT_EQ(serie[8], 4.70);
  EXPECT_EQ(serie[9], 5.60);
  EXPECT_EQ(serie[10], 6.80);
  EXPECT_EQ(serie[11], 8.20);
}

///
/// \brief E24LengthAndValues
/// \test  check if E24 resistor serie values and length are correct
///
TEST(ESeriesTest, E24LengthAndValues)
{
  ESeries serie(ESeries::e24);

  EXPECT_EQ(serie.getElemCnt(), 24);

  EXPECT_EQ(serie.getName(), "E24");

  EXPECT_EQ(serie[0], 1.00);
  EXPECT_EQ(serie[1], 1.10);
  EXPECT_EQ(serie[2], 1.20);
  EXPECT_EQ(serie[3], 1.30);
  EXPECT_EQ(serie[4], 1.54);
  EXPECT_EQ(serie[5], 1.60);
  EXPECT_EQ(serie[6], 1.80);
  EXPECT_EQ(serie[7], 2.00);
  EXPECT_EQ(serie[8], 2.20);
  EXPECT_EQ(serie[9], 2.40);
  EXPECT_EQ(serie[10], 2.70);
  EXPECT_EQ(serie[11], 3.00);
  EXPECT_EQ(serie[12], 3.30);
  EXPECT_EQ(serie[13], 3.60);
  EXPECT_EQ(serie[14], 3.90);
  EXPECT_EQ(serie[15], 4.30);
  EXPECT_EQ(serie[16], 4.70);
  EXPECT_EQ(serie[17], 5.10);
  EXPECT_EQ(serie[18], 5.60);
  EXPECT_EQ(serie[19], 6.20);
  EXPECT_EQ(serie[20], 6.80);
  EXPECT_EQ(serie[21], 7.50);
  EXPECT_EQ(serie[22], 8.20);
  EXPECT_EQ(serie[23], 9.10);
}

///
/// \brief ESeriesTypeInc
///
TEST(ESeriesTest, ESeriesTypeInc)
{
  IESeries::SeriesType serie = ESeries::e12;
  EXPECT_EQ(++serie, ESeries::e24);
  EXPECT_EQ(++serie, ESeries::e12);
}
