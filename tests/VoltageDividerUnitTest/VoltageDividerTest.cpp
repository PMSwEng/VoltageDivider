///
/// \file VoltageDividerTest.h
/// \date 30.11.2016
/// \author Michael Trummer
///
#include <gtest/gtest.h>
#include <cfloat>
#include <cmath>
#include "../../src/VoltageDivider.h"
#include "ESeriesMock.h"

using namespace testing;

///
/// \brief testCalc
/// \test  test for correct calculation
///
TEST(VoltageDivider, testCalc_iec11)
{
  EResistor lowerR(1, EResistor::assignUpperEValue, ESeriesMock());
  EResistor upperR(10, EResistor::assignLowerEValue, ESeriesMock());
  EXPECT_DEATH(VoltageDivider::ResultingResistors res =
                   VoltageDivider::calc(nextafter(0, -1), 5, lowerR, upperR),
               "");
}

TEST(VoltageDivider, testCalc_iec21)
{
  EResistor lowerR(1, EResistor::assignUpperEValue, ESeriesMock());
  EResistor upperR(10, EResistor::assignLowerEValue, ESeriesMock());
  EXPECT_DEATH(VoltageDivider::ResultingResistors res =
                   VoltageDivider::calc(5, nextafter(0, -1), lowerR, upperR),
               "");
}

TEST(VoltageDivider, testCalc_vec51_1)
{
  double values[] = {1.00, 1.20};
  ESeriesMock eSerie;
  eSerie.setElements(values, sizeof(values) / sizeof(values[0]));
  EResistor lowerR(nextafter(0, 1), EResistor::assignUpperEValue, eSerie);
  EResistor upperR(DBL_MAX, EResistor::assignLowerEValue, eSerie);
  double u2 = nextafter(0, 1);
  double u1 = nextafter(u2, 1);

  VoltageDivider::ResultingResistors res =
      VoltageDivider::calc(u1, u2, lowerR, upperR);

  EXPECT_DOUBLE_EQ(res.r1, 1e-322);
  EXPECT_DOUBLE_EQ(res.r2, 1e-322);
}

TEST(VoltageDivider, testCalc_vec51_2)
{
  double values[] = {1.00, 1.20};
  ESeriesMock eSerie;
  eSerie.setElements(values, sizeof(values) / sizeof(values[0]));
  EResistor lowerR(nextafter(0, 1), EResistor::assignUpperEValue, eSerie);
  EResistor upperR(nextafter(lowerR, 1), EResistor::assignLowerEValue, eSerie);
  double u1 = DBL_MAX;
  double u2 = nextafter(u1, 1);

  VoltageDivider::ResultingResistors res =
      VoltageDivider::calc(u1, u2, lowerR, upperR);

  EXPECT_DOUBLE_EQ(res.r1, 1e-322);
  EXPECT_DOUBLE_EQ(res.r2, 1e-322);
}

TEST(VoltageDivider, testCalc_vec51_3)
{
  double values[] = {1.00, 1.20};
  ESeriesMock eSerie;
  eSerie.setElements(values, sizeof(values) / sizeof(values[0]));
  EResistor lowerR(nextafter(0, 1), EResistor::assignUpperEValue, eSerie);
  EResistor upperR(DBL_MAX, EResistor::assignUpperEValue, eSerie);
  double u1 = DBL_MAX;
  double u2 = nextafter(0, 1);

  VoltageDivider::ResultingResistors res =
      VoltageDivider::calc(u1, u2, lowerR, upperR);

  EXPECT_DOUBLE_EQ(res.r1, 1.2e308);
  EXPECT_DOUBLE_EQ(res.r2, 1e-322);
}

TEST(VoltageDivider, testCalc_iec51)
{
  EResistor lowerR(1, EResistor::assignUpperEValue,
                   ESeriesMock(ESeriesMock::e12));
  EResistor upperR(10, EResistor::assignLowerEValue,
                   ESeriesMock(ESeriesMock::e12));
  double u1 = nextafter(nextafter(0, 1), 1);
  double u2 = nextafter(nextafter(u1, 1), 1);

  EXPECT_DEATH(VoltageDivider::ResultingResistors res =
                   VoltageDivider::calc(u1, u2, lowerR, upperR),
               "");
}
