///
/// \file VoltageDividerTest.h
/// \date 30.11.2016
/// \author Michael Trummer
///

#ifndef TESTVD_H
#define TESTVD_H

#include <QObject>
#include <QtTest>
#include <gtest/gtest.h>
#include "../src/VoltageDivider.h"

using namespace testing;

///
/// \brief testDtor
/// \test  test Ctor and Dtor
///
TEST(VoltageDivider, testDtor)
{
  VoltageDivider* vd = new VoltageDivider();
  delete vd;
}

///
/// \brief testDtorPolymorph
/// \test  test Ctor and Dtor in polymorph way
///
TEST(VoltageDivider, testDtorPolymorph)
{
  QObject* vd = new VoltageDivider();
  delete vd;
}

///
/// \brief testCalc
/// \test  test for correct calculation
///
TEST(VoltageDivider, testCalc)
{
  VoltageDivider vd;
  QSignalSpy spy(&vd, &VoltageDivider::calcFinished);
  int callCnt = 0;
  {
    vd.calc(12,5,1.1,9900, EDecade::e12);
    EXPECT_EQ(spy.count(), ++callCnt);
    QList<QVariant> arguments = spy.at(callCnt-1); // take the first signal
    EXPECT_EQ(arguments.count(), 2); // 2 arguments expected for calcFinished fct
    EXPECT_NEAR(arguments[0].toDouble(), 4.7, 0.01);
    EXPECT_NEAR(arguments[1].toDouble(), 3.3, 0.01);
  }
  {
    vd.calc(12,5,1,1000, EDecade::e12);
    EXPECT_EQ(spy.count(), ++callCnt);
    QList<QVariant> arguments = spy.at(callCnt-1); // take the first signal
    EXPECT_EQ(arguments.count(), 2); // 2 arguments expected for calcFinished fct
    EXPECT_NEAR(arguments[0].toDouble(), 4.7, 0.01);
    EXPECT_NEAR(arguments[1].toDouble(), 3.3, 0.01);
  }
  {
    vd.calc(12,5,1e3,1e4, EDecade::e12);
    EXPECT_EQ(spy.count(), ++callCnt);
    QList<QVariant> arguments = spy.at(callCnt-1); // take the first signal
    EXPECT_EQ(arguments.count(), 2); // 2 arguments expected for calcFinished fct
    EXPECT_NEAR(arguments[0].toDouble(), 4700, 0.01);
    EXPECT_NEAR(arguments[1].toDouble(), 3300, 0.01);
  }
  {
    vd.calc(12,11,100,9900, EDecade::e12);
    EXPECT_EQ(spy.count(), ++callCnt);
    QList<QVariant> arguments = spy.at(callCnt-1); // take the first signal
    EXPECT_EQ(arguments.count(), 2); // 2 arguments expected for calcFinished fct
    EXPECT_NEAR(arguments[0].toDouble(), 330, 0.01);
    EXPECT_NEAR(arguments[1].toDouble(), 3900, 0.01);
  }
  {
    vd.calc(12,11,100,1000, EDecade::e12);
    EXPECT_EQ(spy.count(), ++callCnt);
    QList<QVariant> arguments = spy.at(callCnt-1); // take the first signal
    EXPECT_EQ(arguments.count(), 2); // 2 arguments expected for calcFinished fct
    EXPECT_NEAR(arguments[0].toDouble(), 100, 0.01);
    EXPECT_NEAR(arguments[1].toDouble(), 1000, 0.01);
  }
  {
    vd.calc(12,11,100,10000, EDecade::e12);
    EXPECT_EQ(spy.count(), ++callCnt);
    QList<QVariant> arguments = spy.at(callCnt-1); // take the first signal
    EXPECT_EQ(arguments.count(), 2); // 2 arguments expected for calcFinished fct
    EXPECT_NEAR(arguments[0].toDouble(), 330, 0.01);
    EXPECT_NEAR(arguments[1].toDouble(), 3900, 0.01);
  }
}

#endif // TESTVD_H
