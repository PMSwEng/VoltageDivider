#GCOV Settings
QMAKE_CXXFLAGS += -fprofile-arcs -ftest-coverage
QMAKE_LDFLAGS += -fprofile-arcs -ftest-coverage
LIBS += -lgcov

#Allgemeine Einstellungen
TEMPLATE = app
CONFIG += console
CONFIG += thread
CONFIG += c++11
CONFIG -= app_bundle

QT += core
QT += testlib
QT -= gui

HEADERS += \
    ../../src/VoltageDivider.h \
    ../../src/ESeries.h \
    ../../src/EResistor.h \
    ../../src/IESeries.h \
    ESeriesMock.h

SOURCES +=  \
    main.cpp \
    ../../src/VoltageDivider.cpp \
    ../../src/ESeries.cpp \
    ../../src/EResistor.cpp \
    ../../src/ESeriesType.cpp \
    VoltageDividerTest.cpp \
    ESeriesTest.cpp \
    EResistorTest.cpp \
    ESeriesMock.cpp


#GoogleTest Settings
unix|win32: LIBS += -lgtest
