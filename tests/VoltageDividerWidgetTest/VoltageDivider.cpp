///
/// \file   VoltageDividerStub.cpp
/// \date   30.11.2016
/// \author Michael Trummer
///

#include "VoltageDivider.h"

double VoltageDivider::u1;
double VoltageDivider::u2;

ESeries VoltageDivider::eSeries(IESeries::e24);
EResistor VoltageDivider::lowerRTh(1, EResistor::assignUpperEValue, eSeries);
EResistor VoltageDivider::upperRTh(2, EResistor::assignUpperEValue, eSeries);
VoltageDivider::ResultingResistors VoltageDivider::resResistors;
