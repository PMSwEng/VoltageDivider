///
/// \file VoltageDividerWidgetTest/VoltageDivider.h
/// \date 30.11.2016
/// \author Michael Trummer
///

#ifndef VOLTAGEDIVIDER_H
#define VOLTAGEDIVIDER_H

#include <cassert>
#include "EResistor.h"
#include "ESeries.h"

namespace Mock
{
///
/// \brief The VoltageDividerMock class used as mock for unit tests. This class
/// must be a singleton because (additional) attributes are used for Mock
///
class VoltageDivider {
 public:
  ///
  /// \brief The ResultingResistors struct
  ///
  struct ResultingResistors
  {
    ///
    /// \brief r1 represents the upper resistor in an unloaded voltage divider
    ///
    double r1;
    ///
    /// \brief r2 represents the lower resistor in an unloaded voltage divider
    ///
    double r2;
  };

  ///
  /// \brief getInstance function to return reference to VoltageDivider Mock
  /// \return reference of VoltageDivider Mock
  ///
  static VoltageDivider& getInstance()
  {
    static VoltageDivider instance;
    return instance;
  }

  ///
  /// \brief resetInstance used to reset attributes e.g. for a new test
  ///
  static void resetInstance()
  {
    getInstance().u1 = 0;
    getInstance().u2 = 0;
    getInstance().count = 0;
  }

  ///
  /// \brief Calculates the optimal values for R1 and R2. The base formula
  ///        behind this calculation is \f$u1=u2 {r2 \over r2+r1}\f$.
  /// \pre \p u1 > \p u2
  /// \pre \p u2 > 0
  /// \pre \p lowerRTh < \p upperRTh
  /// \pre \p lowerRTh > 0
  /// \param u1 Value for U1 in V
  /// \param u2 Value for U2 in V
  /// \param lowerRTh set the lower bound for resistor value output
  /// \param upperRTh set the higher bound for resistor value output
  ///
  static ResultingResistors calc(double u1, double u2,
                                 const EResistor& lowerRTh,
                                 const EResistor& upperRTh)
  {
    assert(u1 > u2);
    assert(u2 > 0);
    assert(lowerRTh < upperRTh);
    assert(lowerRTh > 0);
    VoltageDivider::u1 = u1;
    VoltageDivider::u2 = u2;
    VoltageDivider::lowerRTh = lowerRTh;
    VoltageDivider::upperRTh = upperRTh;
    getInstance().count++;
    return resResistors;
  }

  ///
  /// \brief setResultingResistors
  /// \param r1
  /// \param r2
  ///
  void setResultingResistors(double r1, double r2)
  {
    resResistors.r1 = r1;
    resResistors.r2 = r2;
  }

  ///
  /// \brief getCount is a getter for test reason
  /// \return how many times the calc funktion is called
  ///
  int getCount() const
  {
    return count;
  }

  ///
  /// \brief getU1 function getter for test reasons
  /// \return value of u1 since last calc function call
  ///
  double getU1() const
  {
    return u1;
  }

  ///
  /// \brief getU2 function getter for test reasons
  /// \return value of u2 since last calc function call
  ///
  double getU2() const
  {
    return u2;
  }

  ///
  /// \brief getLowerRTh function getter for test reasons
  /// \return value of Rmin since last calc function call
  ///
  const EResistor& getLowerRTh() const
  {
    return lowerRTh;
  }

  ///
  /// \brief getUpperRTh function getter for test reasons
  /// \return value of Rmax since last calc function call
  ///
  const EResistor& getUpperRTh() const
  {
    return upperRTh;
  }

 private:
  ///
  /// \brief VoltageDivider Ctor
  ///
  VoltageDivider()
  {
  }

  int count;
  static double u1;
  static double u2;
  static ESeries eSeries;
  static EResistor lowerRTh;
  static EResistor upperRTh;
  static ResultingResistors resResistors;
};
}

using Mock::VoltageDivider;

#endif  // VOLTAGEDIVIDER_H
