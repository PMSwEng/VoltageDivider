///
/// \file   VoltageDividerWidgetTest.cpp
/// \brief  Test-Case for VoltageDividerWidgetTest Unit Test
/// \author Michael Trummer
///
///

// this include must be included before VoltageDivider.h in any way
#include "VoltageDivider.h"

#include <gtest/gtest.h>
#include <QObject>
#include <QtTest>
#include <QtWidgets>
#include "VoltageDividerWidget.h"

///
/// \brief testGUIInput is a helper function to test different input
///        and the corresponding output. The input string should be
///        valid to test against the test parameter.
/// \param strU1 U1 parameter input string
/// \param strU2 U2 parameter input string
/// \param strRMin Rmin parameter input string
/// \param strRMax Rmax parameter input string
/// \param u1 value for u1 to test against
/// \param u2 value for u2 to test against
/// \param rMin value for rMin to test against
/// \param rMax value for rMax to check against
/// \param serie ESeries used to select the appropriate resistor result
///            value.
///
void testGUIInput(const QString& strU1, const QString& strU2,
                  const QString& strRMin, const QString& strRMax, double u1,
                  double u2, double rMin, double rMax,
                  const IESeries::SeriesType& serieType = IESeries::e24)
{
  int callCnt = 0;
  VoltageDivider::resetInstance();
  VoltageDivider::getInstance().setResultingResistors(1, 1);
  VoltageDividerWidget vd;
  QPushButton* pBtn = vd.findChild<QPushButton*>("pushButton");
  ASSERT_TRUE(pBtn != 0);

  QTest::mouseClick(pBtn, Qt::LeftButton);
  EXPECT_EQ(VoltageDivider::getInstance().getCount(), 0);

  QLineEdit* leU1 = vd.findChild<QLineEdit*>("lineEditU1");
  ASSERT_TRUE(leU1 != 0);
  QTest::keyClicks(leU1, strU1);
  QTest::keyPress(leU1, Qt::Key_Enter);
  EXPECT_EQ(VoltageDivider::getInstance().getCount(), 0);

  QLineEdit* leU2 = vd.findChild<QLineEdit*>("lineEditU2");
  ASSERT_TRUE(leU2 != 0);
  QTest::keyClicks(leU2, strU2);

  QComboBox* cmbBox = vd.findChild<QComboBox*>("comboBox");
  ASSERT_TRUE(cmbBox != 0);
  if (cmbBox->currentIndex() != serieType)
  {
    cmbBox->setCurrentIndex(serieType);
    EXPECT_EQ(VoltageDivider::getInstance().getCount(), ++callCnt);
  }
  QLineEdit* leRmin = vd.findChild<QLineEdit*>("lineEditRmin");
  ASSERT_TRUE(leRmin != 0);
  leRmin->setText("");
  QTest::keyClicks(leRmin, strRMin);

  QLineEdit* leRmax = vd.findChild<QLineEdit*>("lineEditRmax");
  ASSERT_TRUE(leRmax != 0);
  leRmax->setText("");
  QTest::keyClicks(leRmax, strRMax);
  QTest::keyPress(leRmax, Qt::Key_Enter);

  EXPECT_EQ(VoltageDivider::getInstance().getCount(), ++callCnt);
  EXPECT_EQ(VoltageDivider::getInstance().getU1(), u1);
  EXPECT_EQ(VoltageDivider::getInstance().getU2(), u2);
  EXPECT_EQ(VoltageDivider::getInstance().getLowerRTh(), rMin);
  EXPECT_EQ(VoltageDivider::getInstance().getUpperRTh(), rMax);
  EXPECT_EQ(VoltageDivider::getInstance().getLowerRTh().getSerie().getType(),
            serieType);

  QTest::mouseClick(pBtn, Qt::LeftButton);
  EXPECT_EQ(VoltageDivider::getInstance().getCount(), ++callCnt);
}

///
/// \brief testCtorDtor
/// \test test Ctor and Dtor
///
TEST(VoltageDividerWidgetTest, testCtorDtor)
{
  VoltageDividerWidget* vd = new VoltageDividerWidget();
  delete vd;
}

///
/// \brief testSuccess
/// \test Test correct input case for U1, U2, Rmin, Rmax
///
TEST(VoltageDividerWidgetTest, testSuccessE24)
{
  testGUIInput("12", "5", "100", "1000", 12, 5, 100, 1000);
}

///
/// \brief testSuccess
/// \test Test correct input case for U1, U2, Rmin, Rmax
///
TEST(VoltageDividerWidgetTest, testSuccessE12)
{
  testGUIInput("12", "5", "100", "1000", 12, 5, 100, 1000, IESeries::e12);
}

///
/// \brief testUnits
/// \test Test all input units for U1, U2, Rmin, Rmax
///
TEST(VoltageDividerWidgetTest, testUnits)
{
  testGUIInput("12V", "5", "100", "1000", 12, 5, 100, 1000);
  testGUIInput("12 V", "5", "100", "1000", 12, 5, 100, 1000);
  testGUIInput("12mV", "5m", "100", "1000", 12e-3, 5e-3, 100, 1000);
  testGUIInput("12 mV", "5 m", "100", "1000", 12e-3, 5e-3, 100, 1000);
  testGUIInput("12 mV", "5 mV", "100", "1000", 12e-3, 5e-3, 100, 1000);
  testGUIInput("12 mV", "5mV", "100", "1000", 12e-3, 5e-3, 100, 1000);
  testGUIInput("12 kV", "5k V", "100", "1000", 12e3, 5e3, 100, 1000);
  testGUIInput("12 kV", "5k V", "100k", "1000k", 12e3, 5e3, 100e3, 1000e3);
  testGUIInput("12 kV", "5k V", "100kOhm", "1000kOhm", 12e3, 5e3, 100e3,
               1000e3);
  testGUIInput("12 kV", "5k V", "100k Ohm", "1000 kOhm", 12e3, 5e3, 100e3,
               1000e3);
  testGUIInput("12 kV", "5k V", "100 k Ohm", "1000kOhm", 12e3, 5e3, 100e3,
               1000e3);
  testGUIInput("12 Kv", "5k V", "100 K oHm", "1000kOHM", 12e3, 5e3, 100e3,
               1000e3);
  testGUIInput("12 Kv", "5k V", "100 M oHm", "1000MOHM", 12e3, 5e3, 100e6,
               1000e6);
}

///
/// \brief testInputDefaults
/// \test Test input defaults Rmin, Rmax
///
TEST(VoltageDividerWidgetTest, testInputDefaults)
{
  int callCnt = 0;
  VoltageDivider::resetInstance();
  VoltageDividerWidget vd;
  QPushButton* pBtn = vd.findChild<QPushButton*>("pushButton");
  ASSERT_TRUE(pBtn != 0);

  QTest::mouseClick(pBtn, Qt::LeftButton);
  EXPECT_EQ(VoltageDivider::getInstance().getCount(), 0);

  QLineEdit* leU1 = vd.findChild<QLineEdit*>("lineEditU1");
  ASSERT_TRUE(leU1 != 0);
  QTest::keyClicks(leU1, "12");
  QTest::keyPress(leU1, Qt::Key_Enter);
  EXPECT_EQ(VoltageDivider::getInstance().getCount(), 0);

  QLineEdit* leU2 = vd.findChild<QLineEdit*>("lineEditU2");
  ASSERT_TRUE(leU2 != 0);
  QTest::keyClicks(leU2, "5");
  QTest::keyPress(leU2, Qt::Key_Enter);
  EXPECT_EQ(VoltageDivider::getInstance().getCount(), ++callCnt);
  EXPECT_EQ(VoltageDivider::getInstance().getU1(), 12);
  EXPECT_EQ(VoltageDivider::getInstance().getU2(), 5);
  EXPECT_EQ(VoltageDivider::getInstance().getLowerRTh(), 1000);
  EXPECT_EQ(VoltageDivider::getInstance().getUpperRTh(), 10000);

  QTest::mouseClick(pBtn, Qt::LeftButton);
  EXPECT_EQ(VoltageDivider::getInstance().getCount(), ++callCnt);
}

///
/// \brief testOutput
/// \test the if output in successful
///
TEST(VoltageDividerWidgetTest, testOutput)
{
  VoltageDivider::resetInstance();
  VoltageDividerWidget vd;

  QLineEdit* leR1 = vd.findChild<QLineEdit*>("lineEditR1");
  ASSERT_TRUE(leR1 != 0);
  UserInputValidator inpValidatorR1("Ohm", &vd);
  leR1->setValidator(&inpValidatorR1);

  QLineEdit* leR2 = vd.findChild<QLineEdit*>("lineEditR2");
  ASSERT_TRUE(leR2 != 0);
  UserInputValidator inpValidatorR2("Ohm", &vd);
  leR2->setValidator(&inpValidatorR2);

  QLineEdit* leVout = vd.findChild<QLineEdit*>("lineEditVout");
  ASSERT_TRUE(leVout != 0);
  UserInputValidator inpValidatorVout("V", &vd);
  leVout->setValidator(&inpValidatorVout);

  QLineEdit* leAcc = vd.findChild<QLineEdit*>("lineEditAccuracy");
  ASSERT_TRUE(leAcc != 0);
  UserInputValidator inpValidatorAcc("%", &vd);
  leAcc->setValidator(&inpValidatorAcc);

  // U1 is set to be able to calculate real Vout
  QLineEdit* leU1 = vd.findChild<QLineEdit*>("lineEditU1");
  ASSERT_TRUE(leU1 != 0);
  QTest::keyClicks(leU1, "12");

  // U2 is set to be able to calculate real Vout
  QLineEdit* leU2 = vd.findChild<QLineEdit*>("lineEditU2");
  ASSERT_TRUE(leU2 != 0);
  QTest::keyClicks(leU2, "3.3");

  // set result and generate calc signal to set U1, U2, Rmin, and Rmax
  VoltageDivider::getInstance().setResultingResistors(470, 180);
  QTest::keyPress(leU2, Qt::Key_Enter);

  // Check R1
  EXPECT_TRUE(inpValidatorR1.getIsValid());
  EXPECT_EQ(inpValidatorR1.getValue(), 470);

  // Check R2
  EXPECT_TRUE(inpValidatorR2.getIsValid());
  EXPECT_EQ(inpValidatorR2.getValue(), 180);

  // Check Vout
  EXPECT_TRUE(inpValidatorVout.getIsValid());
  EXPECT_EQ(inpValidatorVout.getValue(), 3.32);

  // Check Accuracy
  EXPECT_TRUE(inpValidatorAcc.getIsValid());
  EXPECT_EQ(inpValidatorAcc.getValue(), 0.699);
}

///
/// \brief testOutputNegativeAcc
/// \test test output with negative accuracy
///       --> the output must be an absolte value
///
TEST(VoltageDividerWidgetTest, testOutputNegativeAcc)
{
  VoltageDivider::resetInstance();
  VoltageDividerWidget vd;

  QLineEdit* leR1 = vd.findChild<QLineEdit*>("lineEditR1");
  ASSERT_TRUE(leR1 != 0);
  UserInputValidator inpValidatorR1("Ohm", &vd);
  leR1->setValidator(&inpValidatorR1);

  QLineEdit* leR2 = vd.findChild<QLineEdit*>("lineEditR2");
  ASSERT_TRUE(leR2 != 0);
  UserInputValidator inpValidatorR2("Ohm", &vd);
  leR2->setValidator(&inpValidatorR2);

  QLineEdit* leVout = vd.findChild<QLineEdit*>("lineEditVout");
  ASSERT_TRUE(leVout != 0);
  UserInputValidator inpValidatorVout("V", &vd);
  leVout->setValidator(&inpValidatorVout);

  QLineEdit* leAcc = vd.findChild<QLineEdit*>("lineEditAccuracy");
  ASSERT_TRUE(leAcc != 0);
  UserInputValidator inpValidatorAcc("%", &vd);
  leAcc->setValidator(&inpValidatorAcc);

  // U1 is set to be able to calculate real Vout
  QLineEdit* leU1 = vd.findChild<QLineEdit*>("lineEditU1");
  ASSERT_TRUE(leU1 != 0);
  QTest::keyClicks(leU1, "12");

  // U2 is set to be able to calculate real Vout
  QLineEdit* leU2 = vd.findChild<QLineEdit*>("lineEditU2");
  ASSERT_TRUE(leU2 != 0);
  QTest::keyClicks(leU2, "5.04");

  // generate calc signal to set U1, U2, Rmin, and Rmax
  VoltageDivider::getInstance().setResultingResistors(1800, 1300);
  QTest::keyPress(leU2, Qt::Key_Enter);

  // Check Rmin
  EXPECT_TRUE(inpValidatorR1.getIsValid());
  EXPECT_EQ(inpValidatorR1.getValue(), 1800);

  // Check Rmax
  EXPECT_TRUE(inpValidatorR2.getIsValid());
  EXPECT_EQ(inpValidatorR2.getValue(), 1300);

  // Check Vout
  EXPECT_TRUE(inpValidatorVout.getIsValid());
  EXPECT_EQ(inpValidatorVout.getValue(), 5.03);

  // Check Accuracy: must be |value|
  EXPECT_TRUE(inpValidatorAcc.getIsValid());
  EXPECT_EQ(inpValidatorAcc.getValue(), 0.154);
}

///
/// \brief testOutputZeroAcc
/// \test  test output with zero value accuracy
///
TEST(VoltageDividerWidgetTest, testEqualU)
{
  VoltageDivider::resetInstance();
  VoltageDividerWidget vd;

  QLineEdit* leR1 = vd.findChild<QLineEdit*>("lineEditR1");
  ASSERT_TRUE(leR1 != 0);
  UserInputValidator inpValidatorR1("Ohm", &vd);
  leR1->setValidator(&inpValidatorR1);

  QLineEdit* leR2 = vd.findChild<QLineEdit*>("lineEditR2");
  ASSERT_TRUE(leR2 != 0);
  UserInputValidator inpValidatorR2("Ohm", &vd);
  leR2->setValidator(&inpValidatorR2);

  QLineEdit* leVout = vd.findChild<QLineEdit*>("lineEditVout");
  ASSERT_TRUE(leVout != 0);
  UserInputValidator inpValidatorVout("V", &vd);
  leVout->setValidator(&inpValidatorVout);

  QLineEdit* leAcc = vd.findChild<QLineEdit*>("lineEditAccuracy");
  ASSERT_TRUE(leAcc != 0);
  UserInputValidator inpValidatorAcc("%", &vd);
  leAcc->setValidator(&inpValidatorAcc);

  // U1 is set to be able to calculate real Vout
  QLineEdit* leU1 = vd.findChild<QLineEdit*>("lineEditU1");
  ASSERT_TRUE(leU1 != 0);
  QTest::keyClicks(leU1, "0");

  // U2 is set to be able to calculate real Vout
  QLineEdit* leU2 = vd.findChild<QLineEdit*>("lineEditU2");
  ASSERT_TRUE(leU2 != 0);
  QTest::keyClicks(leU2, "0");

  // generate calc signal to set U1, U2, Rmin, and Rmax
  QTest::keyPress(leU2, Qt::Key_Enter);

  // Check R1
  EXPECT_FALSE(inpValidatorR1.getIsValid());

  // Check R2
  EXPECT_FALSE(inpValidatorR2.getIsValid());

  // Check Vout
  EXPECT_FALSE(inpValidatorVout.getIsValid());

  // Check Accuracy
  EXPECT_FALSE(inpValidatorAcc.getIsValid());
}

///
/// \brief testOutputInfAcc
/// \test  test output if infinite accuracy
///
TEST(VoltageDividerWidgetTest, testOutputInfAcc)
{
  VoltageDivider::resetInstance();
  VoltageDividerWidget vd;

  QLineEdit* leR1 = vd.findChild<QLineEdit*>("lineEditR1");
  ASSERT_TRUE(leR1 != 0);
  UserInputValidator inpValidatorR1("Ohm", &vd);
  leR1->setValidator(&inpValidatorR1);

  QLineEdit* leR2 = vd.findChild<QLineEdit*>("lineEditR2");
  ASSERT_TRUE(leR2 != 0);
  UserInputValidator inpValidatorR2("Ohm", &vd);
  leR2->setValidator(&inpValidatorR2);

  QLineEdit* leVout = vd.findChild<QLineEdit*>("lineEditVout");
  ASSERT_TRUE(leVout != 0);
  UserInputValidator inpValidatorVout("V", &vd);
  leVout->setValidator(&inpValidatorVout);

  QLineEdit* leAcc = vd.findChild<QLineEdit*>("lineEditAccuracy");
  ASSERT_TRUE(leAcc != 0);
  UserInputValidator inpValidatorAcc("%", &vd);
  leAcc->setValidator(&inpValidatorAcc);

  // U1 is set to be able to calculate real Vout
  QLineEdit* leU1 = vd.findChild<QLineEdit*>("lineEditU1");
  ASSERT_TRUE(leU1 != 0);
  QTest::keyClicks(leU1, "12");

  // U2 is set to be able to calculate real Vout
  QLineEdit* leU2 = vd.findChild<QLineEdit*>("lineEditU2");
  ASSERT_TRUE(leU2 != 0);
  QTest::keyClicks(leU2, "0.1");

  // generate calc signal to set U1, U2, Rmin, and Rmax
  VoltageDivider::getInstance().setResultingResistors(470, 180);
  QTest::keyPress(leU2, Qt::Key_Enter);

  // Check Rmin
  EXPECT_TRUE(inpValidatorR1.getIsValid());
  EXPECT_EQ(inpValidatorR1.getValue(), 470);

  // Check Rmax
  EXPECT_TRUE(inpValidatorR2.getIsValid());
  EXPECT_EQ(inpValidatorR2.getValue(), 180);

  // Check Vout
  EXPECT_TRUE(inpValidatorVout.getIsValid());
  EXPECT_EQ(inpValidatorVout.getValue(), 3.32);

  // Check Accuracy
  EXPECT_TRUE(inpValidatorAcc.getIsValid());
  EXPECT_EQ(inpValidatorAcc.getValue(), 3220);
}

///
/// \brief testRminInvalid
/// \test Test invalid input for Rmin
///
TEST(VoltageDividerWidgetTest, testRminInvalid)
{
  VoltageDivider::resetInstance();
  VoltageDividerWidget vd;
  QPushButton* pBtn = vd.findChild<QPushButton*>("pushButton");
  ASSERT_TRUE(pBtn != 0);

  QTest::mouseClick(pBtn, Qt::LeftButton);
  EXPECT_EQ(VoltageDivider::getInstance().getCount(), 0);

  QLineEdit* leU1 = vd.findChild<QLineEdit*>("lineEditU1");
  ASSERT_TRUE(leU1 != 0);
  QTest::keyClicks(leU1, "12");
  QTest::keyPress(leU1, Qt::Key_Enter);
  EXPECT_EQ(VoltageDivider::getInstance().getCount(), 0);

  QLineEdit* leU2 = vd.findChild<QLineEdit*>("lineEditU2");
  ASSERT_TRUE(leU2 != 0);
  QTest::keyClicks(leU2, "5");

  QLineEdit* leRmin = vd.findChild<QLineEdit*>("lineEditRmin");
  ASSERT_TRUE(leRmin != 0);
  leRmin->setText("");
  QTest::keyClicks(leRmin, "X");

  QTest::mouseClick(pBtn, Qt::LeftButton);
  EXPECT_EQ(VoltageDivider::getInstance().getCount(), 0);
}

///
/// \brief testRmaxInvalid
/// \test test behavior with an invalid Rmax input
///
TEST(VoltageDividerWidgetTest, testRmaxInvalid)
{
  VoltageDivider::resetInstance();
  VoltageDividerWidget vd;
  QPushButton* pBtn = vd.findChild<QPushButton*>("pushButton");
  ASSERT_TRUE(pBtn != 0);

  QTest::mouseClick(pBtn, Qt::LeftButton);
  EXPECT_EQ(VoltageDivider::getInstance().getCount(), 0);

  QLineEdit* leU1 = vd.findChild<QLineEdit*>("lineEditU1");
  ASSERT_TRUE(leU1 != 0);
  QTest::keyClicks(leU1, "12");
  QTest::keyPress(leU1, Qt::Key_Enter);
  EXPECT_EQ(VoltageDivider::getInstance().getCount(), 0);

  QLineEdit* leU2 = vd.findChild<QLineEdit*>("lineEditU2");
  ASSERT_TRUE(leU2 != 0);
  QTest::keyClicks(leU2, "5");

  QLineEdit* leRmin = vd.findChild<QLineEdit*>("lineEditRmin");
  ASSERT_TRUE(leRmin != 0);
  leRmin->setText("");
  QTest::keyClicks(leRmin, "100");

  QLineEdit* leRmax = vd.findChild<QLineEdit*>("lineEditRmax");
  ASSERT_TRUE(leRmax != 0);
  leRmax->setText("");
  QTest::keyClicks(leRmax, "X");

  QTest::mouseClick(pBtn, Qt::LeftButton);
  EXPECT_EQ(VoltageDivider::getInstance().getCount(), 0);
}

///
/// \brief testRmaxLsRmin
/// \test  test behavior with \f$Rmax < Rmin\f$
///
TEST(VoltageDividerWidgetTest, testRmaxLsRmin)
{
  VoltageDivider::resetInstance();
  VoltageDividerWidget vd;
  QPushButton* pBtn = vd.findChild<QPushButton*>("pushButton");
  ASSERT_TRUE(pBtn != 0);

  QTest::mouseClick(pBtn, Qt::LeftButton);
  EXPECT_EQ(VoltageDivider::getInstance().getCount(), 0);

  QLineEdit* leU1 = vd.findChild<QLineEdit*>("lineEditU1");
  ASSERT_TRUE(leU1 != 0);
  QTest::keyClicks(leU1, "12");
  QTest::keyPress(leU1, Qt::Key_Enter);
  EXPECT_EQ(VoltageDivider::getInstance().getCount(), 0);

  QLineEdit* leU2 = vd.findChild<QLineEdit*>("lineEditU2");
  ASSERT_TRUE(leU2 != 0);
  QTest::keyClicks(leU2, "5");

  QLineEdit* leRmin = vd.findChild<QLineEdit*>("lineEditRmin");
  ASSERT_TRUE(leRmin != 0);
  leRmin->setText("");
  QTest::keyClicks(leRmin, "100");

  QLineEdit* leRmax = vd.findChild<QLineEdit*>("lineEditRmax");
  ASSERT_TRUE(leRmax != 0);
  leRmax->setText("");
  QTest::keyClicks(leRmax, "10");

  QTest::mouseClick(pBtn, Qt::LeftButton);
  EXPECT_EQ(VoltageDivider::getInstance().getCount(), 0);
}

///
/// \brief testRmaxLsRmin
/// \test  test behavior with \f$Rmax = Rmin\f$
///
TEST(VoltageDividerWidgetTest, testRmaxEqRmin)
{
  VoltageDivider::resetInstance();
  VoltageDividerWidget vd;
  QPushButton* pBtn = vd.findChild<QPushButton*>("pushButton");
  ASSERT_TRUE(pBtn != 0);

  QTest::mouseClick(pBtn, Qt::LeftButton);
  EXPECT_EQ(VoltageDivider::getInstance().getCount(), 0);

  QLineEdit* leU1 = vd.findChild<QLineEdit*>("lineEditU1");
  ASSERT_TRUE(leU1 != 0);
  QTest::keyClicks(leU1, "12");
  QTest::keyPress(leU1, Qt::Key_Enter);
  EXPECT_EQ(VoltageDivider::getInstance().getCount(), 0);

  QLineEdit* leU2 = vd.findChild<QLineEdit*>("lineEditU2");
  ASSERT_TRUE(leU2 != 0);
  QTest::keyClicks(leU2, "5");

  QLineEdit* leRmin = vd.findChild<QLineEdit*>("lineEditRmin");
  ASSERT_TRUE(leRmin != 0);
  leRmin->setText("");
  QTest::keyClicks(leRmin, "100");

  QLineEdit* leRmax = vd.findChild<QLineEdit*>("lineEditRmax");
  ASSERT_TRUE(leRmax != 0);
  leRmax->setText("");
  QTest::keyClicks(leRmax, "100");

  QTest::mouseClick(pBtn, Qt::LeftButton);
  EXPECT_EQ(VoltageDivider::getInstance().getCount(), 0);
}

///
/// \brief testU1EqU2
/// \test  test behavior with \f$U1 = U2\f$
///
TEST(VoltageDividerWidgetTest, testU1EqU2)
{
  VoltageDivider::resetInstance();
  VoltageDividerWidget vd;
  QPushButton* pBtn = vd.findChild<QPushButton*>("pushButton");
  ASSERT_TRUE(pBtn != 0);

  QTest::mouseClick(pBtn, Qt::LeftButton);
  EXPECT_EQ(VoltageDivider::getInstance().getCount(), 0);

  QLineEdit* leU1 = vd.findChild<QLineEdit*>("lineEditU1");
  ASSERT_TRUE(leU1 != 0);
  QTest::keyClicks(leU1, "1");
  QTest::keyPress(leU1, Qt::Key_Enter);
  EXPECT_EQ(VoltageDivider::getInstance().getCount(), 0);

  QLineEdit* leU2 = vd.findChild<QLineEdit*>("lineEditU2");
  ASSERT_TRUE(leU2 != 0);
  QTest::keyClicks(leU2, "1");

  QTest::mouseClick(pBtn, Qt::LeftButton);
  EXPECT_EQ(VoltageDivider::getInstance().getCount(), 0);
}

///
/// \brief testU2Eq0
/// \test  test behavior with \f$U2 = 0\f$
///
TEST(VoltageDividerWidgetTest, testU2Eq0)
{
  VoltageDivider::resetInstance();
  VoltageDividerWidget vd;
  QPushButton* pBtn = vd.findChild<QPushButton*>("pushButton");
  ASSERT_TRUE(pBtn != 0);

  QTest::mouseClick(pBtn, Qt::LeftButton);
  EXPECT_EQ(VoltageDivider::getInstance().getCount(), 0);

  QLineEdit* leU1 = vd.findChild<QLineEdit*>("lineEditU1");
  ASSERT_TRUE(leU1 != 0);
  QTest::keyClicks(leU1, "1");
  QTest::keyPress(leU1, Qt::Key_Enter);
  EXPECT_EQ(VoltageDivider::getInstance().getCount(), 0);

  QLineEdit* leU2 = vd.findChild<QLineEdit*>("lineEditU2");
  ASSERT_TRUE(leU2 != 0);
  QTest::keyClicks(leU2, "0");

  QTest::mouseClick(pBtn, Qt::LeftButton);
  EXPECT_EQ(VoltageDivider::getInstance().getCount(), 0);
}
