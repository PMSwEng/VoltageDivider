#GCOV Settings
QMAKE_CXXFLAGS += -fprofile-arcs -ftest-coverage
QMAKE_LDFLAGS += -fprofile-arcs -ftest-coverage
LIBS += -lgcov

#Allgemeine Einstellungen
TEMPLATE = app
CONFIG += console
CONFIG += thread
CONFIG += c++11
CONFIG -= app_bundle

QT += core
QT += gui widgets
QT += testlib
QT -= gui

HEADERS +=     \
    ../../src/UserInputValidator.h \
    VoltageDividerWidget.h \
    ../../src/IESeries.h \
    VoltageDivider.h \
    ESeries.h \
    EResistor.h

SOURCES +=   \
    main.cpp \
    UserInputValidator.cpp \
    ESeriesType.cpp \
    VoltageDividerWidget.cpp \
    VoltageDividerWidgetTest.cpp \
    VoltageDivider.cpp \
    EResistor.cpp


FORMS    += \
     ../../src/voltagedividerwidget.ui

#GoogleTest Settings
unix|win32: LIBS += -lgtest
