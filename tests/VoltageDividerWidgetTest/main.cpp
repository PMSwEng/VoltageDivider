///
/// \file   main.cpp
/// \date   30.11.2016
/// \author Michael Trummer
///

#include <QApplication>
#include <gtest/gtest.h>

int main(int argc, char *argv[])
{
  QApplication app(argc, argv);

  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
